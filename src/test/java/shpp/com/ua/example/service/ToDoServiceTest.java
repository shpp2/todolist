package shpp.com.ua.example.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import shpp.com.ua.example.model.State;
import shpp.com.ua.example.model.Task;
import shpp.com.ua.example.repository.ToDoRepository;
import shpp.com.ua.example.repository.UserRepository;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@Slf4j
class ToDoServiceTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    ToDoRepository toDoRepository;
    @Autowired
    UserRepository userRepository;
    private Task firstTask;
    private Task secondTask;
    private String firstTaskCurrentTime;
    private String secondTaskCurrentTime;

    @BeforeEach
    void createTestUser() {
        this.firstTask = new Task();
        this.firstTask.setToDoTask(" My first task ... ");
        this.firstTask.setState(State.PLANNED);
        this.firstTask.setUserId(2);
        this.firstTask.setExecutionTime(LocalDateTime.of(2023, 3, 3, 10, 0));
        toDoRepository.save(firstTask);

        this.secondTask = new Task();
        this.secondTask.setToDoTask(" My second task ... ");
        this.secondTask.setState(State.PLANNED);
        this.secondTask.setUserId(2);
        this.secondTask.setExecutionTime(LocalDateTime.of(2023, 3, 3, 10, 0));
        toDoRepository.save(secondTask);

        this.firstTaskCurrentTime = toDoRepository.findById((long) 1).get().getCurrentTime().toString();
        this.secondTaskCurrentTime = toDoRepository.findById((long) 2).get().getCurrentTime().toString();
    }

    @Test
    void showAllTestResponseCorrectJsonsArray() throws Exception {
        RequestBuilder requestBuilder = get("/todo");
        this.mockMvc
                .perform(requestBuilder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{" +
                        "\"create_time\":\"" + firstTaskCurrentTime + "\"," +
                        "\"id\":1," +
                        "\"todo_task\":\" My first task ... \"," +
                        "\"state\":\"PLANNED\"," +
                        "\"user_id\":2," +
                        "\"execution_time\":\"2023-03-03T10:00:00\"" +
                        "}," +
                        "{" +
                        "\"create_time\":\"" + secondTaskCurrentTime + "\"," +
                        "\"id\":2," +
                        "\"todo_task\":\" My second task ... \"," +
                        "\"state\":\"PLANNED\"," +
                        "\"user_id\":2," +
                        "\"execution_time\":\"2023-03-03T10:00:00\"" +
                        "}]"));
    }

    @Test
    void showTaskTest() throws Exception {
        RequestBuilder requestBuilder = get("/todo/1")
                .header("Authorization", "Basic YWRtaW46cGFzc3dvcmQ=")
                .header("Cookie", "JSESSIONID=925B1576B7A48538A3E67DF123A27082");
        this.mockMvc
                .perform(requestBuilder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{" +
                        "\"create_time\":\"" + firstTaskCurrentTime + "\"," +
                        "\"id\":1," +
                        "\"todo_task\":\" My first task ... \"," +
                        "\"state\":\"PLANNED\"," +
                        "\"user_id\":2," +
                        "\"execution_time\":\"2023-03-03T10:00:00\"" +
                        "}"));
    }

    @Test
    void showAllTasksForOneUserTest() throws Exception {
        RequestBuilder requestBuilder = get("/todo/user")
                .header("Authorization", "Basic YWRtaW46cGFzc3dvcmQ=")
                .header("Cookie", "JSESSIONID=925B1576B7A48538A3E67DF123A27082");
        this.mockMvc
                .perform(requestBuilder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{" +
                        "\"create_time\":\"" + firstTaskCurrentTime + "\"," +
                        "\"id\":1," +
                        "\"todo_task\":\" My first task ... \"," +
                        "\"state\":\"PLANNED\"," +
                        "\"user_id\":2," +
                        "\"execution_time\":\"2023-03-03T10:00:00\"" +
                        "}," +
                        "{" +
                        "\"create_time\":\"" + secondTaskCurrentTime + "\"," +
                        "\"id\":2," +
                        "\"todo_task\":\" My second task ... \"," +
                        "\"state\":\"PLANNED\"," +
                        "\"user_id\":2," +
                        "\"execution_time\":\"2023-03-03T10:00:00\"" +
                        "}]"));
    }

    @Test
    void createTaskTest() throws Exception {
        RequestBuilder requestBuilder = post("/todo")
                .header("Authorization", "Basic YWRtaW46cGFzc3dvcmQ=")
                .header("Content-Type", "application/json")
                .header("Cookie", "JSESSIONID=925B1576B7A48538A3E67DF123A27082")
                .content("{" +
                        "\"todo_task\": \" My next task ... \", " +
                        "\"state\": \"PLANNED\"," +
                        "\"current_time\": \"2023-01-29T17:00:00\"," +
                        "\"execution_time\": \"2023-04-03T10:00:00\"}");
        this.mockMvc
                .perform(requestBuilder)
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void updateTaskTest() throws Exception {
        RequestBuilder requestBuilder = put("/todo")
                .header("Authorization", "Basic YWRtaW46cGFzc3dvcmQ=")
                .header("Content-Type", "application/json")
                .header("Cookie", "JSESSIONID=925B1576B7A48538A3E67DF123A27082")
                .content("{" +
                        "\"id\": \"1\"," +
                        "\"todo_task\": \" My first task ... \"," +
                        "\"state\": \"WORK_IN_PROGRESS\"," +
                        "\"execution_time\": \"2023-03-03T11:00:00\"," +
                        "\"create_time\": \"2023-03-03T10:00:00\"" +
                        "}");
        this.mockMvc
                .perform(requestBuilder)
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void deleteTaskTest() throws Exception {
        firstTask.setState(State.DONE);
        toDoRepository.save(firstTask);
        secondTask.setState(State.DONE);
        toDoRepository.save(secondTask);
        RequestBuilder requestBuilder = delete("/todo/1?lang=uk")
                .header("Authorization", "Basic YWRtaW46cGFzc3dvcmQ=")
                .header("Cookie", "JSESSIONID=40FBCB372137FA217C583B7198642F3F");
        this.mockMvc
                .perform(requestBuilder)
                .andDo(print())
                .andExpect(status().isOk());
    }

}