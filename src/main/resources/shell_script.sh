#!/bin/sh

java -jar ToDoList-1.0.4.jar &

curl -X 'POST' \
  'http://localhost:5000/person' \
  -H 'accept: */*' \
  -H 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' \
  -H 'Content-Type: application/json' \
  -d '{
  "username": "super_admin",
  "password": "$2a$12$hXqxN/VFuimOF5sTqhQcPOxMud6qWgePRiSXWRkOAtb.jkhVQiz9q",
  "roles": [
    "ADMIN"
  ],
  "enabled": true
}'

curl -X 'POST' \
  'http://localhost:5000/person' \
  -H 'accept: */*' \
  -H 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' \
  -H 'Content-Type: application/json' \
  -d '{
  "username": "super_puper",
  "password": "$2a$12$hXqxN/VFuimOF5sTqhQcPOxMud6qWgePRiSXWRkOAtb.jkhVQiz9q",
  "roles": [
    "SUPER_USER"
  ],
  "enabled": true
}'

curl -X 'PUT' \
  'http://localhost:5000/person' \
  -H 'accept: */*' \
  -H 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' \
  -H 'Content-Type: application/json' \
  -d '{
  "username": "super_admin",
  "password": "$2a$12$hXqxN/VFuimOF5sTqhQcPOxMud6qWgePRiSXWRkOAtb.jkhVQiz9q",
  "roles": [
    "ADMIN", "USER"
  ],
  "enabled": true
}'

curl -X 'PUT' \
  'http://localhost:5000/person' \
  -H 'accept: */*' \
  -H 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' \
  -H 'Content-Type: application/json' \
  -d '{
  "username": "super_admin",
  "password": "$2a$12$hXqxN/VFuimOF5sTqhQcPOxMud6qWgePRiSXWRkOAtb.jkhVQiz9q",
  "roles": [
    "ADMIN", "SUPER_USER"
  ],
  "enabled": true
}'

curl --location --request POST 'http://localhost:5000/todo?lang=uk' \
--header 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=556BCF7CB8F5AA83835EAF38D33584B3' \
--data-raw '{
  "todo_task": " My first task ... ",
  "state": "PLANNED",
  "execution_time": "2023-03-03T11:00:00",
  "create_time": "2023-03-03T10:00:00"
}'

curl --location --request POST 'http://localhost:5000/todo?lang=uk' \
--header 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=556BCF7CB8F5AA83835EAF38D33584B3' \
--data-raw '{
  "todo_task": " My second task ... ",
  "state": "PLANNED",
  "execution_time": "2023-03-03T11:00:00",
  "create_time": "2023-03-03T10:00:00"
}'

curl --location --request POST 'http://localhost:5000/todo?lang=uk' \
--header 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=556BCF7CB8F5AA83835EAF38D33584B3' \
--data-raw '{
  "todo_task": " My third task ... ",
  "state": "PLANNED",
  "execution_time": "2023-03-03T11:00:00",
  "create_time": "2023-03-03T10:00:00"
}'

curl --location --request PUT 'http://localhost:5000/todo?lang=uk' \
--header 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=556BCF7CB8F5AA83835EAF38D33584B3' \
--data-raw '{
        "id": 1,
        "todo_task": " My first task ... ",
        "state": "DONE",
        "user_id": 2,
        "create_time": "2023-03-03T10:00:00",
        "execution_time": "2023-03-03T11:00:00"
    }'

