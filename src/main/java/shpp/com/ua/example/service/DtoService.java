package shpp.com.ua.example.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import shpp.com.ua.example.dto.TaskDto;
import shpp.com.ua.example.dto.TaskDtoId;
import shpp.com.ua.example.dto.UserDto;
import shpp.com.ua.example.model.Role;
import shpp.com.ua.example.model.Task;
import shpp.com.ua.example.model.User;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class DtoService {
    private DtoService(){
    }

    public static Task createEntityFromDto(TaskDto taskDto) {
        return Task.builder()
                .toDoTask(taskDto.getToDoTask())
                .state(taskDto.getState())
                .userId(taskDto.getUserId())
                .currentTime(taskDto.getCurrentTime())
                .executionTime(taskDto.getExecutionTime())
                .build();
    }

    public static Task createEntityFromDto(TaskDtoId taskDtoResponse) {

        return Task.builder()
                .id(taskDtoResponse.getId())
                .toDoTask(taskDtoResponse.getToDoTask())
                .state(taskDtoResponse.getState())
                .userId(taskDtoResponse.getUserId())
                .currentTime(taskDtoResponse.getCurrentTime())
                .executionTime(taskDtoResponse.getExecutionTime())
                .build();
    }

    public static TaskDtoId createDtoResponseFromEntity(Task task) {
        return TaskDtoId.builder()
                .id(task.getId())
                .toDoTask(task.getToDoTask())
                .state(task.getState())
                .userId(task.getUserId())
                .currentTime(task.getCurrentTime())
                .executionTime(task.getExecutionTime())
                .build();
    }

    public static User createUserFromUserDto(UserDto userDto) {
        return User.builder()
                .username(userDto.getUsername())
                .password(userDto.getPassword())
                .roles(getRolesFromUserDto(userDto))
                .enabled(userDto.isEnabled())
                .build();
    }

    private static List<Role> getRolesFromUserDto(UserDto userDto) {
        List<String> dtoRoles = userDto.getRoles();
        List<Role> roles = new ArrayList<>();
        for (String dtoRole : dtoRoles) {
            roles.add(Role.builder().name(dtoRole).build());
        }
        return roles;
    }

    public static UserDto createUserDtoFromUser(User user) {
        return UserDto.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .roles(getRolesFromUser(user))
                .enabled(user.isEnabled())
                .build();
    }

    private static List<String> getRolesFromUser(User user) {
        List<Role> roles = user.getRoles();
        List<String> dtoRoles = new ArrayList<>();
        for (Role role : roles) {
            dtoRoles.add(role.getName());
        }
        return dtoRoles;
    }

    public static User updateUserFromDB(User user, UserDto userDto) {
        user.setPassword(userDto.getPassword());
        user.setRoles(DtoService.getRolesFromUserDto(userDto));
        user.setEnabled(userDto.isEnabled());
        return user;
    }

}
