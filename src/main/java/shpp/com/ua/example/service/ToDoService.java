package shpp.com.ua.example.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.webjars.NotFoundException;
import shpp.com.ua.example.dto.ResponseMessage;
import shpp.com.ua.example.dto.TaskDto;
import shpp.com.ua.example.dto.TaskDtoId;
import shpp.com.ua.example.model.State;
import shpp.com.ua.example.model.Task;
import shpp.com.ua.example.model.User;
import shpp.com.ua.example.repository.ToDoRepository;
import shpp.com.ua.example.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Slf4j
@RequiredArgsConstructor
public class ToDoService {

    private final ToDoRepository toDoRepository;
    private final UserRepository userRepository;
    private final MessageSource messageSource;
    private static final String ERROR_MESSAGE_TASK = "error.message.task";
    private static final String ERROR_MESSAGE_STATE = "error.message.state";

    public List<TaskDtoId> showAll() {
        List<TaskDtoId> taskDtoList = new ArrayList<>();
        toDoRepository.findAll().forEach(task -> taskDtoList.add(DtoService.createDtoResponseFromEntity(task)));
        return taskDtoList;
    }

    public TaskDtoId showTask(String username, long taskId, Locale locale) {
        if (userRepository.existsByUsername(username)) {
            if (toDoRepository.existsById(taskId)) {
                return DtoService.createDtoResponseFromEntity(toDoRepository.findById(taskId).get());
            } else {
                log.info("SHOW ERROR : Task id {} not found in DB!", taskId);
                throw new NotFoundException(messageSource.getMessage(ERROR_MESSAGE_TASK, null, locale));
            }
        } else {
            log.info("SHOW ERROR : User with username {} not found in DB!", username);
            throw new NotFoundException(messageSource.getMessage(ERROR_MESSAGE_TASK, null, locale));
        }
    }

    public List<TaskDtoId> showAllTasksForOneUser(String username, Locale locale) {
        if (userRepository.existsByUsername(username)) {
            List<TaskDtoId> taskDtoList = new ArrayList<>();
            toDoRepository.findAll().forEach(task -> taskDtoList.add(DtoService.createDtoResponseFromEntity(task)));
            return taskDtoList;
        } else {
            log.info("SHOW ERROR : User with username {} not found in DB!", username);
            throw new NotFoundException(messageSource.getMessage("error.message.userDB", null, locale));
        }
    }

    public ResponseMessage create(TaskDto taskDto, String username, BindingResult bindingResult, Locale locale) {
        if (bindingResult.hasErrors()) {
            log.info("TaskDto {} is not valid! Please input valid task!", taskDto.toString());
            return createResponseMessage(locale, HttpStatus.BAD_REQUEST, "error.message.validate");
        }
        taskDto.setUserId(userRepository.findByUsername(username).getId());
        Task taskEntity = DtoService.createEntityFromDto(taskDto);
        if (taskEntity.getState().equals(State.PLANNED)) {
            log.info("start create Task ... ");
            toDoRepository.save(taskEntity);
            log.info("SUCCESS REQUEST! for createTask method");
            return createResponseMessage(locale, HttpStatus.CREATED, "info.message.task.create");
        } else {
            log.info("CREATE ERROR : Task`s state {} is not valid ... ", taskDto.getState());
            return createResponseMessage(locale, HttpStatus.BAD_REQUEST, ERROR_MESSAGE_STATE);
        }
    }

    public ResponseMessage update(TaskDtoId taskDtoResponse, String username, BindingResult bindingResult, Locale locale) {
        if (bindingResult.hasErrors()) {
            log.info("UPDATE ERROR : TaskDto {} is not valid ... ", taskDtoResponse.toString());
            return createResponseMessage(locale, HttpStatus.BAD_REQUEST, "error.message.validate");
        }
        taskDtoResponse.setUserId(userRepository.findByUsername(username).getId());
        Task taskEntity = DtoService.createEntityFromDto(taskDtoResponse);
        if (toDoRepository.existsById(taskDtoResponse.getId())) {
            if (taskEntity.getState().ordinal() > toDoRepository.findById(taskDtoResponse.getId()).get().getState().ordinal()) {
                toDoRepository.save(taskEntity);
                return createResponseMessage(locale, HttpStatus.CREATED, "info.message.task.add");
            } else {
                log.info("UPDATE ERROR : Task`s state {} is not valid ... ", taskEntity.getState());
                return createResponseMessage(locale, HttpStatus.BAD_REQUEST, ERROR_MESSAGE_STATE);
            }
        } else {
            log.info("UPDATE ERROR : Task`s ID {} not found in DB!", taskDtoResponse.getId());
            return createResponseMessage(locale, HttpStatus.BAD_REQUEST, ERROR_MESSAGE_TASK);
        }
    }

    public ResponseMessage delete(long taskId, String username, Locale locale) {
        if (toDoRepository.existsById(taskId)) {
            Task task = toDoRepository.findById(taskId).get();
            User user = userRepository.findByUsername(username);
            if (task.getUserId() == user.getId()) {
                if (task.getState().equals(State.DONE) || task.getState().equals(State.CANCELLED)) {
                    toDoRepository.deleteById(taskId);
                    return createResponseMessage(locale, HttpStatus.OK, "info.message.task.del");
                } else {
                    log.info("DELETE ERROR : State of task {} is not valid!", task.getState());
                    return createResponseMessage(locale, HttpStatus.BAD_REQUEST, ERROR_MESSAGE_STATE);
                }
            } else {
                log.info("DELETE ERROR : Username {} in DB and in task is not equals!", username);
                return createResponseMessage(locale, HttpStatus.BAD_REQUEST, "error.message.user");
            }
        } else {
            log.info("DELETE ERROR : Task id {} not found in DB!", taskId);
            return createResponseMessage(locale, HttpStatus.BAD_REQUEST, ERROR_MESSAGE_TASK);
        }
    }

    public ResponseMessage deleteAll(long userId, Locale locale) {
        if (userRepository.existsById(userId)) {
            List<Long> list = getUserEndTasks(userId);
            for (Long taskNumber : list) {
                toDoRepository.deleteById(taskNumber);
            }
            return createResponseMessage(locale, HttpStatus.OK, "info.message.task.del");
        } else {
            log.info("DELETE ERROR : User id {} not found in DB!", userId);
            return createResponseMessage(locale, HttpStatus.BAD_REQUEST, "error.message.userDB");
        }
    }

    private List<Long> getUserEndTasks(long userId) {
        List<Long> list = new ArrayList<>();
        toDoRepository.findAll().forEach(task -> {
            if (task.getUserId() == userId && (
                    task.getState().equals(State.DONE) || task.getState().equals(State.CANCELLED))) {
                list.add(task.getId());
            }
        });
        return list;
    }

    private ResponseMessage createResponseMessage(Locale locale, HttpStatus status, String source) {
        return new ResponseMessage(
                LocalDateTime.now(),
                status,
                messageSource.getMessage(source, null, locale));
    }
}
