package shpp.com.ua.example.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import shpp.com.ua.example.repository.RoleRepository;

@Slf4j
@Service
@RequiredArgsConstructor
@Getter
public class RoleService {
    private final RoleRepository roleRepository;

}
