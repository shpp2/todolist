package shpp.com.ua.example.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import shpp.com.ua.example.dto.UserDto;
import shpp.com.ua.example.model.User;
import shpp.com.ua.example.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public List<UserDto> showAllUser() {
        List<UserDto> usersDto = new ArrayList<>();
        userRepository.findAll().forEach(user -> usersDto.add(DtoService.createUserDtoFromUser(user)));
        return usersDto;
    }

    public UserDto showUserById(long id) {
        if (userRepository.existsById(id)) {
            return DtoService.createUserDtoFromUser(userRepository.findById(id).get());
        }
        log.info("SHOW ERROR : User ID {} not found in DB!", id);
        return null;
    }

    public boolean saveUser(UserDto userDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            log.info(" SAVE ERROR : Your request is not valid! Pleas try again! ");
            return false;
        }
        if (userRepository.existsByUsername(userDto.getUsername())) {
            log.info("SAVE ERROR : This username {} is already in the database!", userDto.getUsername());
            return false;
        }
        userRepository.save(DtoService.createUserFromUserDto(userDto));
        return true;
    }

    public boolean updateUser(UserDto userDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            log.info(" UPDATE ERROR : Your request is not valid! Pleas try again! ");
            return false;
        }
        if (userRepository.existsByUsername(userDto.getUsername())) {
            User user = userRepository.findByUsername(userDto.getUsername());
            userRepository.save(DtoService.updateUserFromDB(user, userDto));
            return true;
        }
        log.info("UPDATE ERROR : This username {} is not exist in the database!", userDto.getUsername());
        return false;
    }

    public boolean delete(long id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
            return true;
        }
        log.info(" DELETE ERROR : User ID {} not found in DB! ", id);
        return false;
    }
}
