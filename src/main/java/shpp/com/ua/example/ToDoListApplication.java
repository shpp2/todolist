package shpp.com.ua.example;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import shpp.com.ua.example.model.Role;
import shpp.com.ua.example.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@Slf4j
@OpenAPIDefinition(info = @Info(title = "Restful API App", version = "${api.ver}",
		description = "This program implements the Restful API, and allows you " +
				"to add valid users to the database, as well as receive information about them."))
public class ToDoListApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToDoListApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(UserRepository users, PasswordEncoder encoder) {
		Role user = new Role();
		user.setName("USER");
		Role admin = new Role();
		admin.setName("ADMIN");

		List<Role> roles = new ArrayList<>();
		roles.add(user);
		List<Role> roles1 = new ArrayList<>();
		roles1.add(admin);

		return args -> {
			users.save(new shpp.com.ua.example.model.User("user", encoder.encode("password"), roles, true));
			users.save(new shpp.com.ua.example.model.User("admin", encoder.encode("password"), roles1, true));
		};
	}

}
