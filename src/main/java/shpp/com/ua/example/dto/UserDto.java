package shpp.com.ua.example.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Builder
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Schema UserDto", description = "User TaskDto, for receiving data from the client")
public class UserDto {
    @NotEmpty(message = "Name should not be empty!")
    @Size(min = 2, message = "Name must contain at least two characters!")
    private String username;
    @NotEmpty(message = "Password should not be empty!")
    @Size(min = 5, message = "Password must contain at least five characters!")
    private String password;
    @NotNull
    private List<String> roles;
    private boolean enabled;
}
