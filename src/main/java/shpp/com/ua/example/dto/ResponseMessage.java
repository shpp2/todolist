package shpp.com.ua.example.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@Schema(name = "Schema ResponseMessage", description = "ResponseMessage is a transfer object for inform the frontend developer")
public class ResponseMessage {
    @JsonProperty(value = "current_time")
    private LocalDateTime currentTime;
    @JsonProperty(value = "http_status")
    private HttpStatus httpStatus;
    @JsonProperty(value = "message")
    private String message;
}
