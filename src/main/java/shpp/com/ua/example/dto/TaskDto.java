package shpp.com.ua.example.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import shpp.com.ua.example.model.State;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
@Builder
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Schema TaskDto", description = "Scheme TaskDto, for receiving data from the client")
public class TaskDto {
    @NotEmpty(message = "Task should not be empty!")
    @JsonProperty(value = "todo_task")
    private String toDoTask;
    @NotNull
    @JsonProperty(value = "state")
    private State state;
    @JsonProperty(value = "user_id")
    private long userId;
    @JsonProperty(value = "create_time")
    LocalDateTime currentTime = LocalDateTime.now();
    @JsonProperty(value = "execution_time")
    LocalDateTime executionTime;
}
