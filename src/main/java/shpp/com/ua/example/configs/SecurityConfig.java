package shpp.com.ua.example.configs;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import shpp.com.ua.example.service.JpaUserDetailsService;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@SecurityScheme(name = "todo_security_api", scheme = "basic", type = SecuritySchemeType.HTTP, in = SecuritySchemeIn.HEADER)
public class SecurityConfig {

  private final JpaUserDetailsService myUserDetailsService;
  private static final String ROLE_ADMIN = "ADMIN";
  private static final String ROLE_USER = "USER";
  private static final String URL_USER = "/person";
  private static final String URL_TODO = "/todo";

  @Bean
  @Order(1)
  SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    return http
        .csrf().disable()
        .authorizeRequests()
        .antMatchers(HttpMethod.GET, URL_USER, URL_TODO).hasAuthority(ROLE_ADMIN)
        .antMatchers(HttpMethod.GET, URL_USER + "/{id}", URL_TODO + "/{taskId}", URL_TODO + "/user")
        .hasAnyAuthority(ROLE_ADMIN, ROLE_USER)
        .antMatchers(HttpMethod.POST, URL_USER, URL_TODO).hasAnyAuthority(ROLE_ADMIN, ROLE_USER)
        .antMatchers(HttpMethod.PUT, URL_USER + "/{id}").hasAuthority(ROLE_ADMIN)
        .antMatchers(HttpMethod.DELETE, URL_USER + "/{id}", URL_TODO + "/user/{userId}")
        .hasAuthority(ROLE_ADMIN)
        .antMatchers(HttpMethod.PUT, URL_TODO).hasAnyAuthority(ROLE_ADMIN, ROLE_USER)
        .antMatchers(HttpMethod.DELETE, URL_TODO + "/{taskId}")
        .hasAnyAuthority(ROLE_ADMIN, ROLE_USER)
        .and()
        .userDetailsService(myUserDetailsService)
        .headers(headers -> headers.frameOptions().sameOrigin())
        .httpBasic()
        .and()
        .build();
  }

  @Bean
  PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

}
