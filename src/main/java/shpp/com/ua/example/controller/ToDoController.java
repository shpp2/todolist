package shpp.com.ua.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.security.Principal;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shpp.com.ua.example.dto.ResponseMessage;
import shpp.com.ua.example.dto.TaskDto;
import shpp.com.ua.example.dto.TaskDtoId;
import shpp.com.ua.example.service.ToDoService;

@RestController
@Slf4j()
@RequiredArgsConstructor
@SecurityRequirement(name = "todo_security_api")
@RequestMapping("/todo")
public class ToDoController {
    private final ToDoService toDoService;

    @GetMapping()
//    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(description = "Get list of tasks from DB", summary = "Get list of tasks from DB")
    public ResponseEntity<List<TaskDtoId>> getAllTasks() {
        return ResponseEntity.ok(toDoService.showAll());
    }

    @GetMapping("/{taskId}")
//    @PreAuthorize("hasAuthority({'USER','ADMIN'})")
    @Operation(description = "Get task for from DB", summary = "Get task from DB")
    public ResponseEntity<TaskDtoId> getTask(@PathVariable Long taskId, Principal principal, Locale locale) {
        return ResponseEntity.ok(toDoService.showTask(principal.getName(), taskId, locale));
    }

    @GetMapping("/user")
//    @PreAuthorize("hasAuthority({'USER','ADMIN'})")
    @Operation(description = "Get list of tasks for one user from DB", summary = "Get list of tasks for one user from DB")
    public ResponseEntity<List<TaskDtoId>> getAllTasksForOneUser(Principal principal, Locale locale) {
        return ResponseEntity.ok(toDoService.showAllTasksForOneUser(principal.getName(), locale));
    }

    @PostMapping()
//    @PreAuthorize("hasAuthority({'USER','ADMIN'})")
    @Operation(description = "Create task in DB", summary = "Create taskEntity from taskDto in DB")
    public ResponseEntity<ResponseMessage> createTask(@Valid @RequestBody TaskDto taskDto, Principal principal,
                                                      BindingResult bindingResult, Locale locale) {
        log.info(" Start createTask with POST method ... ");
        ResponseMessage responseMessage = toDoService.  create(taskDto, principal.getName(), bindingResult, locale);
        return response(HttpStatus.CREATED, responseMessage);
    }

    @PutMapping()
//    @PreAuthorize("hasAuthority({'USER','ADMIN'})")
    @Operation(description = "Update task from DB", summary = "Update taskEntity from taskDto in DB")
    public ResponseEntity<ResponseMessage> updateTask(@Valid @RequestBody TaskDtoId taskDtoResponse, Principal principal,
                                                      BindingResult bindingResult, Locale locale) {
        log.info(" Start updateTask with PUT method ... ");
        ResponseMessage responseMessage = toDoService.update(taskDtoResponse, principal.getName(), bindingResult, locale);
        return response(HttpStatus.CREATED, responseMessage);
    }

    @DeleteMapping("/{taskId}")
//    @PreAuthorize("hasAuthority({'USER','ADMIN'})")
    @Operation(description = "Delete task from DB", summary = "Delete task from DB")
    public ResponseEntity<ResponseMessage> deleteTask(@PathVariable Long taskId, Principal principal, Locale locale) {
        log.info(" Start deleteTask with DELETE method ...");
        ResponseMessage responseMessage = toDoService.delete(taskId, principal.getName(), locale);
        return response(HttpStatus.OK, responseMessage);
    }

    @DeleteMapping("/user/{userId}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(description = "Delete all task for user ID from DB", summary = "Delete all task for user ID from DB")
    public ResponseEntity<ResponseMessage> deleteAllTasksForUserId(@PathVariable long userId, Locale locale) {
        log.info(" Start deleteAllTasksForUserId with DELETE method ...");
        ResponseMessage responseMessage = toDoService.deleteAll(userId, locale);
        return response(HttpStatus.OK, responseMessage);
    }

    private ResponseEntity<ResponseMessage> response(HttpStatus status, ResponseMessage responseMessage) {
        if (responseMessage.getHttpStatus().equals(status)) {
            return ResponseEntity.status(status).body(responseMessage);
        }
        return ResponseEntity.badRequest().body(responseMessage);
    }
}
