package shpp.com.ua.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import shpp.com.ua.example.dto.ResponseMessage;
import shpp.com.ua.example.dto.UserDto;
import shpp.com.ua.example.service.UserService;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

@RestController
@Tag(name = "User Controller",
        description = "This is the controller for recording and " +
                "receiving from the DB valid users with a unique TIN")
@Slf4j
@RequestMapping("/person")
@SecurityRequirement(name = "todo_security_api")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final MessageSource messageSource;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(description = "Get all user from DB", summary = "Get all user from DB")
    public ResponseEntity<List<UserDto>> getAllUser(Locale locale) {
        log.info("start getAllUser with GET method ...");
        List<UserDto> listOfUserDto = userService.showAllUser();
        if (listOfUserDto != null) {
            return ResponseEntity.ok(listOfUserDto);
        }
        throw new NotFoundException(messageSource.getMessage("error.message.empty", null, locale));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority({'USER', 'ADMIN'})")
    @Operation(description = "Returns the user by id from DB", summary = "Returns the user by id from DB")
    public ResponseEntity<UserDto> getUser(@PathVariable("id") int id, Locale locale) {
        log.info("start getUser with GET method ...");
        UserDto userDto = userService.showUserById(id);
        if (userDto != null) {
            return ResponseEntity.ok(userDto);
        }
        throw new NotFoundException(messageSource.getMessage("error.message.userDB", null, locale));
    }

    @PostMapping
    @PreAuthorize("hasAuthority({'USER','ADMIN'})")
    @Operation(description = "Create valid user in DB", summary = "Create valid user in DB")
    public ResponseEntity<ResponseMessage> createUser(@Valid @RequestBody UserDto userDto, BindingResult bindingResult,
                                                      Locale locale) {
        log.info("Start create user with POST method ...");
        if (userService.saveUser(userDto, bindingResult)) {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(createResponseMessage(locale, HttpStatus.CREATED, "info.message.user.add"));
        }
        return ResponseEntity.badRequest()
                .body(createResponseMessage(locale, HttpStatus.CREATED, "error.message.user.fail"));
    }

    private ResponseMessage createResponseMessage(Locale locale, HttpStatus status, String source) {
        return new ResponseMessage(
                LocalDateTime.now(),
                status,
                messageSource.getMessage(source, null, locale));
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(description = "Makes changes to user fields in the DB", summary = "Makes changes to user fields in the DB")
    public ResponseEntity<ResponseMessage> updateUser(@Valid @RequestBody UserDto userDto, BindingResult bindingResult,
                                                      Locale locale) {
        log.info("Start update user with UPDATE method ...");
        if (userService.updateUser(userDto, bindingResult)) {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(createResponseMessage(locale, HttpStatus.CREATED, "info.message.user.add"));
        }
        return ResponseEntity.badRequest()
                .body(createResponseMessage(locale, HttpStatus.CREATED, "error.message.user.fail"));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(description = "Delete the user by id from DB", summary = "Returns the user by id from DB")
    public ResponseEntity<ResponseMessage> deleteUser(@PathVariable("id") int id, Locale locale) {
        log.info("Start delete user with DELETE method ...");
        if (userService.delete(id)) {
            return ResponseEntity.ok(createResponseMessage(locale, HttpStatus.CREATED, "info.message.user.del"));
        } else {
            throw new NotFoundException(messageSource.getMessage("error.message.userDB", null, locale));
        }
    }
}
