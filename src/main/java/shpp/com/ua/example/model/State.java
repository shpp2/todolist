package shpp.com.ua.example.model;

public enum State {
    PLANNED,
    WORK_IN_PROGRESS ,
    DONE,
    CANCELLED
}
