package shpp.com.ua.example.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "todo_task")
    private String toDoTask;
    private State state;
    @Column(name = "user_id")
    private long userId;
    @Column(name = "create_time")
    LocalDateTime currentTime = LocalDateTime.now();
    @Column(name = "execution_time")
    LocalDateTime executionTime;
}