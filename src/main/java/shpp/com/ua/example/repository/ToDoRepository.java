package shpp.com.ua.example.repository;

import org.springframework.data.repository.CrudRepository;
import shpp.com.ua.example.model.Task;

public interface ToDoRepository extends CrudRepository<Task, Long> {
}
