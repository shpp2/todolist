package shpp.com.ua.example.repository;

import org.springframework.data.repository.CrudRepository;
import shpp.com.ua.example.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
    boolean existsByUsername(String username);

}
