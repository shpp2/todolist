package shpp.com.ua.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shpp.com.ua.example.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
    boolean existsByName(String name);
}
